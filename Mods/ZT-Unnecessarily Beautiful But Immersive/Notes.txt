
 Unnecessarily Beautiful But Immersive - Version 4.5.0.0
 A21 "Living Better" Update
 Notes Log 4.5.0.0
 
 
 ================================
 
 
 
 CREATIVE & DEVELOPER MENUS
 ==============
 
 To access the creative menu and the developer menu, you must select F1 and then type "DM" for Developer Mode and enter and then type "CM" for Creative Menu and enter. Once done and both are activated, press F1 again and it'll back you out of the admin screen.
 
 
 CONFIG DUMP LOCATION
 ==============
 The default location is \AppData\Roaming\7DaysToDie\Saves\[MAP NAME]\[GAME NAME]\ConfigsDump
 
 Type "exportcurrentconfigs" into your game console to locate your specific path to your Config Dump if not using the default location.
 
 
 
 ================================
 
 
 
 UNITY VERSIONS
 ==============
 
 The original assets of UBB was using Unity version 2018.2.21f1. I was provided the original package Unity files by Hernan himself to bring all the assets into modern day Unity version 2021.3.19f1. This newer version is compatible with A21 and provides the best quality looking versions of all items.
 
 To open/start a project using the older version, you must navigate to the drive the project is located in and then go to Unity. Open the respective version you're looking for and then choose "Editor." From there, you can select "Unity.exe" and it should open the older version of Unity. Only after saving the files in the older Editor can you then open the project in the new Unity Hub and also open it in the new version.
 
 Once all files are converted to the newest version of Unity that is compatible with the latest alpha of 7 Days to Die can you then bundle using the LZ4 method.
 
 
 UNITY ANIMATIONS
 ==============
 
 The "doors animating upon placement" and then closing issue occurs when you are using a non-unique animation state names when it's sitting in a big project.
 
 Changing "Open," "OpenStatic," "Closed" and "ClosedStatic" to Open1, OpenStatic1, Closed1 and ClosedStatic1 fixed the issue.
 
 
 
 ================================
 
 
 
 SORTORDER1 & SORTORDER2
 ==============
 
 Context for Sort Order Conversation: https://discord.com/channels/243577046616375297/350402983336607754/1127831567520256020
 
 All individual blocks received SortOrder1 ZT100 and SortOrder2 from "00001" to "00244."

 I then put SortOrder1 ZT100 and SortOrder2 from "00245" to "00256" to force the Variant Blocks to the end of the Creative Menu (CM).

 Putting those two properties on each individual block (not masters or grandmasters) and then the variant blocks with ascending SortOrder2 with only one SortOrder1 ZT100, I was able to finally correctly order all blocks.
 
 You should be able to put anything in place of "ZT" of "ZT100" such as "Z100, BA200, A100" etc. etc.
 
 
 
 ================================
 
 
 
 MATERIALS MASTER LIST
 ==============
 
 Mair
 Mwater

 MbarbedFence
 MbarbedWire
 MtrapSpikesWood
 MtrapSpikesIron
 MDynamite
 MLandMine
 MFuelBarrelMetal
 MFuelBarrelPolymer 

 Morganic
 Mdirt
 MforestGround
 Mfarmland
 MfertilizedFarmland
 MfertilizedClayPlanter
 Mplants
 Mleaves
 Mmushrooms
 Mcactus
 Mcorn
 Mtallgrass
 
 Mbridge_asphalt
 Mbridge_metal
 Mbridge_concrete
 Mbridge_wood
 Mgravel
 Mrubble
 
 Mstone
 MstoneFurniture
 Mconcrete
 MrConcrete
 Mbedrock
 MoreStone
 MoreMetal
 Mbrick
 Mstone_scrap
 
 Miron_scrap
 Mbrass_scrap
 Mlead_scrap
 Mmetal_weak
 Mmetal_chains
 Mmetal_frame
 Mmetal_rebar
 Mmetal_thin
 Mmetal
 Mmetal_medium
 Mmetal_hard
 MmetalDrawbridge
 MmetalGarageDoor3x2x2
 MmetalGarageDoor5x3x1
 MmetalCatwalk
 Miron
 Msteel
 MstainlessSteel
 
 MDesertGround
 Msand
 Msandstone
 Mhay
 Mpaper
 Mtrash
 Mleather
 MleatherKnuckles
 
 Mwood
 Mwood_regular
 Mwood_weak
 Mwood_ladders
 MwoodReinforced
 MwoodMetal
 MwoodCatwalk
 MtreeWoodSmall
 MtreeWoodMedium
 MtreeWoodLarge
 MtreeWoodFull
 Mcloth
 MclothStable
 
 Mglass
 MglassBulletproof
 
 Msnow
 Mweb
 MCardboardLoot
 Mfurniture
 Mbrass
 MdroppedBackpack
 
 Mboulder
 Mcobblestone
 MmetalNoGlue
 MstoneNoGlue
 
 Mplastics
 MelectricParts
 MmechanicalParts
 Mpaint
 
 MMachineGunParts
 MBowCrossbowParts
 MRifleParts
 MHandGunParts
 MShotgunParts
 MRocketLauncherParts
 MmeleeWpnBatonParts
 MmeleeWpnKnucklesParts
 MmeleeWpnBladeParts
 MmeleeWpnSpearParts
 MmeleeWpnClubParts
 MmeleeWpnSledgeParts
 MmeleeToolAllSteel
 MarmorMilitarySet
 MarmorSteelSet
 MMotorToolParts
 MJunkTurretParts
 
 MresourceRockSmall 
 MresourceSnowBall
 MresourceAcid
 MresourceYuccaFibers
 
 MresourceWood
 MresourceClayLump
 MresourceCrushedSand
 MresourceBrokenGlass
 MresourceScrapIron
 MresourceForgedIron
 MresourceMetalPipe
 MresourceForgedSteel
 MresourceScrapPolymers
 MresourceOilShale
 MresourceBone
 MresourceLeather
 
 MfoodRawMeat
 MresourceAnimalFat
 MresourceTestosteroneExtract
 
 MresourceCrop
 MresourceScrapBrassSmall
 MresourceScrapBrassMedium
 MresourceScrapBrassLarge
 MresourceScrapLeadSmall
 MresourceScrapLeadMedium
 MresourceScrapLeadLarge
 
 MresourceCobblestones
 MresourceCement
 MresourceConcreteMix
 
 MresourceCloth
 MmilitaryFiber
 MresourcePaper
 MresourceFeather
 
 MresourceCoal
 MresourceOil
 MresourceSilverNugget
 
 MsmallEngine
 
 MoldCash
 
 MtoolBeaker
 MtoolBellows
 
 
 
 ================================
 
 
 
 CRAFTING TAB NAMES
 ==============
 
 Basics
 Building
 Resources
 Ammo/Weapons
 Tools/Traps
 Food / Cooking
 Science
 Clothing
 Decor
 Miscellaneous
 
 
 To add the properties in a block, you would simply insert one line such as:
 
 <property name="Group" value="Science"/> - or - <property name="Group" value="Building,Basics" /> 
 
 This is just an example.
 
 
 You can also put:
 
 <property name="Group" value="Building,Basics,Science"/>
 
 
 
 ================================
 
 
 
 BLOCK PROPERTIES CONVERSION IMMERSION CHEAT SHEET
 ==============
 
 
	MWOOD_WEAK
	=======
	
		<block name="weakWoodWithGlassExample">
			<property name="Material" value="Mwood_weak" />
			<property name="MaxDamage" value="235"/>
			<property class="RepairItems">
				<property name="resourceWood" value="10" />
			</property>
			<drop event="Destroy" name="resourceWood" count="1,5" />
			<drop event="Destroy" name="resourceBrokenGlass" count="1,3" />
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
			<drop event="Fall" name="resourceBrokenGlass" count="3" prob="0.20" stick_chance="0"/>
		</block>
	
	
	MWOOD_REGULAR
	=======
	
		<block name="regularWoodExample">
			<property name="Material" value="Mwood_regular" />
			<property name="MaxDamage" value="200"/>
			<property class="RepairItems">
				<property name="resourceWood" value="10" />
			</property>
			<drop event="Destroy" name="resourceWood" count="1,5" />
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
		</block>
	
	
	MWOODREINFORCED
	=======
	
		<block name="reinforcedWoodExample">
			<property name="Material" value="MwoodReinforced" />
			<property name="MaxDamage" value="250"/>
			<property class="RepairItems">
				<property name="resourceWood" value="10" />
			</property>
			<drop event="Destroy" name="resourceWood" count="1,5" />
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
		</block>
	
	
	MIRON
	=======
	
		<block name="ironExample">
			<property name="Material" value="Miron" />
			<property name="MaxDamage" value="200"/>
			<property class="RepairItems">
				<property name="resourceForgedIron" value="5" />
			</property>
			<drop event="Destroy" name="resourceScrapIron" count="6" />
			<drop event="Fall" name="resourceScrapIron" count="3" prob="0.20" stick_chance="0"/>
		</block>
	
	
	MGLASS
	=======
	
		<block name="glassExample">
			<property name="Material" value="Mglass" />
			<property name="MaxDamage" value="200"/>
			<property class="RepairItems">
				<property name="glassBlockVariantHelper" value="3" />
			</property>
			<drop event="Destroy" name="resourceBrokenGlass" count="5" />
			<drop event="Fall" name="resourceBrokenGlass" count="7" prob="0.20" stick_chance="0"/>
		</block>
 
 
 BLOCK PROPERTIES CONVERSION IMMERSION CHEAT SHEET FOR WRENCH TOOL LINE
 ==============
 
 
	SINKS
	=======
	
		<block name="sinkExample">
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="250"/>
			<property class="RepairItems">
				<property name="resourceWood" value="10" />
			</property>
			<drop event="Destroy" name="resourceWood" count="5" />
			<drop event="Destroy" name="resourceScrapIron" count="2,4" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMetalPipe" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
		</block>
	
	
	LIGHTS
	=======
	
		<block name="lightExample">
			<property name="Material" value="Miron" />
			<property name="MaxDamage" value="335"/>
			<property class="RepairItems">
				<property name="resourceForgedIron" value="5" />
			</property>
			<drop event="Destroy" name="resourceScrapIron" count="6" />
			<drop event="Destroy" name="resourceHeadlight" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceElectricParts" count="1,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceScrapIron" count="3" prob="0.20" stick_chance="0"/>
		</block>
	
	
	TVS (TVS W/WOOD)
	=======
	
		<block name="tvSetExample">
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="300"/>
			<property class="RepairItems">
				<property name="resourceWood" value="10" />
			</property>
			<drop event="Destroy" name="resourceWood" count="2,6" />
			<drop event="Destroy" name="resourceElectricParts" count="1,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceScrapIron" count="0,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="0,3" prob="0.20" stick_chance="0"/>
			<drop event="Fall" name="resourceScrapIron" count="0,2" prob="0.20" stick_chance="0"/>
		</block>
	
	
	TVS (TVS W/METAL)
	=======
	
		<block name="tvSetTwoExample">
			<property name="Material" value="Mmetal_thin" />
			<property name="MaxDamage" value="250"/>
			<property class="RepairItems">
				<property name="resourceForgedIron" value="5" />
			</property>
			<drop event="Destroy" name="resourceScrapIron" count="2,6" />
			<drop event="Destroy" name="resourceElectricParts" count="1,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceScrapIron" count="0,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceScrapIron" count="0,2" prob="0.20" stick_chance="0"/>
		</block>
	
	
	SOUND BAR / PC / LAPTOP / FIREPLACE
	=======
	
		<block name="techExample">
			<property name="Material" value="Mmetal_thin" />
			<property name="MaxDamage" value="150"/>
			<property class="RepairItems">
				<property name="resourceForgedIron" value="1"/>
			</property>
			<drop event="Destroy" name="resourceScrapIron" count="1,4" />
			<drop event="Destroy" name="resourceElectricParts" count="1,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceScrapIron" count="0,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceScrapIron" count="0,2" prob="0.20" stick_chance="0"/>
		</block>
	
	
	DOORS
	=======
	
		<block name="doorExample">
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="1200"/>
			<property class="RepairItems">
				<property name="resourceForgedIron" value="10" />
			</property>
			<property name="CustomUpgradeSound" value="place_block_wood" />
			<drop event="Destroy" name="resourceWood" count="10" />
			<drop event="Destroy" name="resourceNail" count="5,6" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMechanicalParts" count="0,1" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="5" prob="0.75" stick_chance="1" />
		</block>
	
	
	APPLIANCES (FRIDGE, STOVE, OVEN, TOASTER, COFFEE MAKER, MICROWAVE)
	=======
	
		<block name="appliancesExample">
			<property name="Material" value="Miron" />
			<property name="MaxDamage" value="400"/>
			<property class="RepairItems">
				<property name="resourceForgedIron" value="5" />
			</property>
			<drop event="Destroy" name="resourceScrapIron" count="0,4" prob="1"/>
			<drop event="Destroy" name="resourceElectricParts" count="2,5" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMetalPipe" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceSpring" count="0,2" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceScrapIron" count="3" prob="0.20" stick_chance="0" />
		</block>
	
	
	BEDROOM BLOCKS (DRAWERS & RACKS)
	=======

		<block name="generalDrawerExample">
			<property name="Material" value="MwoodReinforced" />
			<property name="MaxDamage" value="225"/> (250 bigger / 225 smaller)
			<property class="RepairItems">
				<property name="resourceWood" value="10" />
			</property>
			<drop event="Destroy" name="resourceWood" count="1,5" />
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
		</block>
	
	
	SOFA BLOCKS
	=======
	
		<block name="furnitureExample">
			<property name="Material" value="Mleather" />
			<property name="MaxDamage" value="125"/>
			<property class="RepairItems">
				<property name="resourceLeather" value="3" />
			</property>
			<drop event="Destroy" name="resourceLeather" count="1,4" prob="1"/>
			<drop event="Fall" name="resourceNail" count="5" prob="0.20" stick_chance="0" />
			<drop event="Fall" name="resourceLeather" count="4" prob="0.20" stick_chance="0" />
		</block>
 
 
 
 ================================
 
 
 
 Meshfile Information
 ==============
 
 
	CODE EXAMPLE
	=======
	
			<property name="Meshfile" value="#@modfolder:Resources/kitchenModels.unity3d?CabinetA01.prefab"/>
			<property name="DropMeshfile" value="#@modfolder:Resources/kitchenModels.unity3d?CabinetA01.prefab"/>
	
	GUPPYCUR EXPLANATION
	=======
	
	Meshfile is the item in your hand when you have it selected.
	Drop mesh is what it looks like on the ground when you drop it.
 
 
 
 ================================
 
 
 
 INFO FOR ENABLING STORAGE
 ==============
 
 
	K1NGER1988 NOTES
	=======
	
	If you want to adjust the current storage/container blocks all you need to do is go into blocks.xml and look for <property name="LootList" value="6" /> and change the 6 to poiStorageBox and it should show storage options.
	You can also replace with <property name="LootList" value="playerStorage"/> which will give you a lot more slots, but a bit overkill.
	
	OZMODS NOTES
	=======
	
	If you want to get them to work, go into the Blocks.xml and find the cabinets; change the LootList on each to a different value.

	I did;

	"<property name="LootList" value="17" />     to     <property name="LootList" value="cupboard" />   


	This gives the cabinets/sinks/other blocks that you give that LootList value the ability to open/close. I went with cupboard as the value as it gives them 18 slots. If you'd prefer a larger number of slots, you could use "playerStorage" for 48 slot, or even "storageCrate" for 72 slots. 

	The reason they broke in A20 is because the developers moved away from using numbers as IDs for loot containers
 
 
 
 ================================
 
 
 
 CODE BACKUP FOR REMOVED ITEMS
 ==============
 
 
	DECORATION SPHERES
	=======
	
		<!-- 3,3 -->
		<block name="ZT_DecorationSpheresX2">
		<!-- Localization: ZT_DecorationSpheresX2,Decoration Spheres -->
			<property name="Extends" value="ztWoodBasedParentBlocks"/>
			<property name="Material" value="Mglass" />
			<property name="MaxDamage" value="50"/>
			<property name="CanPickup" value="true"/>
			<property name="Model" value="#@modfolder:Resources/decorModels.unity3d?ZTDecor03.prefab"/>
			<property name="CustomIcon" value="ZT_DecoSpheres2" />
			<property class="RepairItems">
				<property name="glassBlockVariantHelper" value="1" />
			</property>
			<drop event="Destroy" name="resourceBrokenGlass" count="1,3" />
			<drop event="Fall" name="resourceBrokenGlass" count="2" prob="0.20" stick_chance="0"/>
			<property name="FilterTags" value="floot" />
			<property name="SortOrder1" value="ZT3000"/>
			<property name="SortOrder2" value="000003"/>
		</block>
	
	
	COFFEE TABLE
	=======
	
		<!-- 77,4 -->
		<block name="ZT_CoffeeTableAX3">
		<!-- Localization: ZT_CoffeeTableAX3,Coffee Table -->
			<property name="Extends" value="ztWoodBasedParentBlocks"/>
			<property name="Material" value="MwoodReinforced" />
			<property name="MaxDamage" value="200"/>
			<property name="MultiBlockDim" value="3,1,1" />
			<property name="Model" value="#@modfolder:Resources/tablesChairsModels.unity3d?ZTTable04.prefab"/>
			<property name="CustomIcon" value="ZT_CoffeeTable3" />
			<property class="RepairItems">
				<property name="resourceWood" value="5" />
			</property>
			<drop event="Destroy" name="resourceWood" count="1,5" />
			<drop event="Destroy" name="resourceBrokenGlass" count="1,3" />
			<drop event="Fall" name="resourceWood" count="1" prob="0.20" stick_chance="0" />
			<drop event="Fall" name="resourceBrokenGlass" count="2" prob="0.20" stick_chance="0" />
			<property name="FilterTags" value="floot" />
			<property name="SortOrder1" value="ZT4000"/>
			<property name="SortOrder2" value="000005"/>
		</block>
	
	
	COFFEE TABLE
	=======

		<!-- 80,4 -->
		<block name="ZT_CoffeeTableX3">
		<!-- Localization: ZT_CoffeeTableX3,Coffee Table -->
			<property name="Extends" value="ztWoodBasedParentBlocks"/>
			<property name="Material" value="MwoodReinforced" />
			<property name="MaxDamage" value="200"/>
			<property name="MultiBlockDim" value="3,1,1" />
			<property name="Model" value="#@modfolder:Resources/tablesChairsModels.unity3d?ZTTable07.prefab"/>
			<property name="CustomIcon" value="ZT_CoffeeTable6" />
			<property class="RepairItems">
				<property name="resourceWood" value="5" />
			</property>
			<drop event="Destroy" name="resourceWood" count="1,5" />
			<drop event="Destroy" name="resourceBrokenGlass" count="1,3" />
			<drop event="Fall" name="resourceWood" count="1" prob="0.20" stick_chance="0" />
			<drop event="Fall" name="resourceBrokenGlass" count="2" prob="0.20" stick_chance="0" />
			<property name="FilterTags" value="floot" />
			<property name="SortOrder1" value="ZT4000"/>
			<property name="SortOrder2" value="000008"/>
		</block>
	
	
	SECRET CLOSET DOOR
	=======
	
		<!-- 187,11 -->
		<block name="ZT_ClosetSecretDoor">
		<!-- Localization - ZT_ClosetSecretDoor,Secret Closet Door -->
			<property name="Extends" value="ztDoorParentBlocks"/>
			<property name="MaxDamage" value="80"/>
			<property name="Model" value="#@modfolder:Resources/doorModels.unity3d?ZTDoor12.prefab"/>
			<property name="CustomIcon" value="ZT_ClosetDoorSecret" />
			<property class="RepairItems">
				<property name="resourceWood" value="10" />
			</property>
			<drop event="Destroy" name="resourceWood" count="1,5" />
			<drop event="Destroy" name="resourceNail" count="2,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="3" prob="0.75" stick_chance="1" />
			<property name="FilterTags" value="floot" />
			<property name="SortOrder1" value="ZT9200"/>
			<property name="SortOrder2" value="000012"/>
     	</block>
 
 
 
 ================================
 
 
 
 A20 Progression Outdated Code
 ==============
 
 	ADVANCED ENGINEERING PERKS
	=======
 
		<append xpath="/progression/perks/perk[@name='perkAdvancedEngineering']">
		
			<effect_group>
			
				<passive_effect name="RecipeTagUnlocked" operation="base_set" level="3,5" value="1" tags="HN_Lights"/>
			
			</effect_group>
		
		</append>
 
 
 
 ================================
 
 
 
 INDIVIDUAL BLOCK TESTING
 ==============
 
 	CHIMNEY POI
	=======
		
	<!-- Individual Blocks -->
		
		<insertAfter xpath="/blocks/block[@name='chimneyBlackPOI']/property[@name='Model']">
		
			<property name="CreativeMode" value="Dev"/>
		
		</insertAfter>
 
 
 
 ================================
 
 
 
 BUFFS NOTES
 ==============
 
 	BUFFELEMENTWET NOTES
	=======
		
	<!--
				
		<display_value value="$WetTempModifierDisplay"/>
		<display_value_key value="{0:0.0}%"/>
				
	-->
 
		<!-- <triggered_effect trigger="onSelfBuffUpdate" action="ModifyCVar" cvar="$WetTempModifierActive" operation="subtract" value=".2"/> -->
 
 
 ================================
 
 
 
 MASTERLIST OF RECIPES FOR CAMPFIRE WORKSTATIONS
 ==============
	
	
	HIERARCHY TRICKLE DOWN OF NEEDS
	==============
	
	ZT_TOASTER GETS:
	==============
	
	foodTunaFishGravyToast
	
	
	ZT_COFFEEMAKER GETS:
	==============
	
	drinkJarBoiledWater
	drinkJarPureMineralWater
	drinkJarCoffee
	drinkJarBlackStrapCoffee
	drinkJarGoldenRodTea
	drinkJarGrandpasAwesomeSauce
	drinkJarGrandpasLearningElixir
	drinkJarGrandpasMoonshine
	drinkJarRedTea
	drinkJarYuccaJuice
	
	ZT_OVEN GETS:
	==============
	
	foodBaconAndEggs
	foodBakedPotato
	foodBlueberryPie
	foodPumpkinPie
	foodPumpkinCheesecake
	foodPumpkinBread
	foodBoiledMeat
	foodCanSham
	foodCharredMeat
	foodChiliDog
	foodCornBread
	foodCornOnTheCob
	foodEggBoiled
	foodFishTacos
	foodGrilledMeat
	foodHoboStew
	foodMeatStew
	foodGumboStew
	foodShepardsPie
	foodSpaghetti
	foodShamChowder
	foodSteakAndPotato
	foodVegetableStew
	
	ZT_MICROWAVE, ZT_STOVE & ZT_STOVETOP GETS:
	==============
	
	drinkJarBoiledWater
	drinkJarPureMineralWater
	drinkJarCoffee
	drinkJarBlackStrapCoffee
	drinkJarGoldenRodTea
	drinkJarGrandpasAwesomeSauce
	drinkJarGrandpasLearningElixir
	drinkJarGrandpasMoonshine
	drinkJarRedTea
	drinkJarYuccaJuice
	foodBaconAndEggs
	foodBakedPotato
	foodBlueberryPie
	foodPumpkinPie
	foodPumpkinCheesecake
	foodPumpkinBread
	foodBoiledMeat
	foodCanSham
	foodCharredMeat
	foodChiliDog
	foodCornBread
	foodCornOnTheCob
	foodEggBoiled
	foodFishTacos
	foodGrilledMeat
	foodHoboStew
	foodMeatStew
	foodGumboStew
	foodShepardsPie
	foodSpaghetti
	foodShamChowder
	foodSteakAndPotato
	foodVegetableStew
	
	CAMPFIRE GETS:
	==============
	
	drinkJarBoiledWater
	drinkJarPureMineralWater
	drinkJarCoffee
	drinkJarBlackStrapCoffee
	drinkJarGoldenRodTea
	drinkJarGrandpasAwesomeSauce
	drinkJarGrandpasLearningElixir
	drinkJarGrandpasMoonshine
	drinkJarRedTea
	drinkJarYuccaJuice
	foodBaconAndEggs
	foodBakedPotato
	foodBlueberryPie
	foodPumpkinPie
	foodPumpkinCheesecake
	foodPumpkinBread
	foodBoiledMeat
	foodCanSham
	foodCharredMeat
	foodChiliDog
	foodCornBread
	foodCornOnTheCob
	foodEggBoiled
	foodFishTacos
	foodGrilledMeat
	foodHoboStew
	foodMeatStew
	foodGumboStew
	foodShepardsPie
	foodSpaghetti
	foodShamChowder
	foodSteakAndPotato
	foodVegetableStew
	foodTunaFishGravyToast
	resourcePaint
	resourceGlue
	drugAntibiotics
	drugHerbalAntibiotics
 
 
 
 ================================
 
 
 
 MASTERLIST OF RECIPES FOR CAMPFIRE WORKSTATIONS
 ==============
	
	
	FAILED DEW COLLECTOR SINKS DUE TO REQUIREMENT OF BEING OUTSIDE
	==============
	
	sinkTip_title,"Sinks"
	ZTSinkDesc,"Collect murky water throughout the day from sinks you've built.\n\nYou will still have to boil the water, but here's an alternative to quenching that thirst!"
	sinkTip,"Quench your thirst by building select sinks that will collect murky water that is ready to be boiled."
	
		<block name="ZT_Sink">
			<property name="Extends" value="ztKitchenParentBlocks"/>
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="250"/>
			<property name="Model" value="#@modfolder:Resources/kitchenModels.unity3d?ZTSink01.prefab"/>
			<!-- Class -->
			<property name="Class" value="DewCollector"/>
			<property name="MinConvertTime" value="3600" /> <!-- 21600 Game Seconds = 6 Game Hours -->
			<property name="MaxConvertTime" value="7200" /> <!-- 36000 Game Seconds = 10 Game Hours -->
			<property name="ConvertToItem" value="drinkJarRiverWater" />
			<!-- UI_Info -->
			<property name="CustomIcon" value="ZT_Sink" />
			<property name="DescriptionKey" value="ZTSinkDesc"/>
			<property name="PickupJournalEntry" value="sinkTip"/>
			<property name="WorkstationIcon" value="ui_game_symbol_water"/>
			<property name="WorkstationJournalTip" value="waterTip"/>
			<!--Interaction -->
			<property name="HeatMapStrength" value="1"/>
			<property name="HeatMapTime" value="1200"/>
			<property name="HeatMapFrequency" value="25"/>
			<property name="OpenSound" value="open_workbench"/>
			<property name="CloseSound" value="close_workbench"/>
			<property name="ConvertSound" value="bucketfill_water"/>
			<!-- Destruction_Info -->
			<property class="RepairItems">
				<property name="resourceForgedIron" value="2" />
			</property>
			<drop event="Destroy" name="resourceWood" count="5" />
			<drop event="Destroy" name="resourceScrapIron" count="2,4" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMetalPipe" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
			<!-- Sorting_Order -->
			<property name="Group" value="Building,advBuilding,Basics,Food/Cooking"/>
			<property name="FilterTags" value="MC_building,SC_loot,SC_plumbing"/>
			<property name="SortOrder1" value="ZT1000"/>
			<property name="SortOrder2" value="000001"/>
		</block>
		
		<block name="ZT_SinkWhiteWood">
			<property name="Extends" value="ztKitchenParentBlocks"/>
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="250"/>
			<property name="Model" value="#@modfolder:Resources/kitchenModels.unity3d?ZTSink02.prefab"/>
			<!-- Class -->
			<property name="Class" value="DewCollector"/>
			<property name="MinConvertTime" value="3600" /> <!-- 21600 Game Seconds = 6 Game Hours -->
			<property name="MaxConvertTime" value="7200" /> <!-- 36000 Game Seconds = 10 Game Hours -->
			<property name="ConvertToItem" value="drinkJarRiverWater" />
			<!-- UI_Info -->
			<property name="CustomIcon" value="ZT_SinkWhiteWood" />
			<property name="DescriptionKey" value="ZTSinkDesc"/>
			<property name="PickupJournalEntry" value="sinkTip"/>
			<property name="WorkstationIcon" value="ui_game_symbol_water"/>
			<property name="WorkstationJournalTip" value="waterTip"/>
			<!--Interaction -->
			<property name="HeatMapStrength" value="1"/>
			<property name="HeatMapTime" value="1200"/>
			<property name="HeatMapFrequency" value="25"/>
			<property name="OpenSound" value="open_workbench"/>
			<property name="CloseSound" value="close_workbench"/>
			<property name="ConvertSound" value="bucketfill_water"/>
			<!-- Destruction_Info -->
			<property class="RepairItems">
				<property name="resourceForgedIron" value="2" />
			</property>
			<drop event="Destroy" name="resourceWood" count="5" />
			<drop event="Destroy" name="resourceScrapIron" count="2,4" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMetalPipe" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
			<!-- Sorting_Order -->
			<property name="Group" value="Building,advBuilding,Basics,Food/Cooking"/>
			<property name="FilterTags" value="MC_building,SC_loot,SC_plumbing"/>
			<property name="SortOrder1" value="ZT1000"/>
			<property name="SortOrder2" value="000008"/>
		</block>
	
 		<block name="ZT_SinkClearWood">
			<property name="Extends" value="ztKitchenParentBlocks"/>
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="250"/>
			<property name="Model" value="#@modfolder:Resources/kitchenModels.unity3d?ZTSink03.prefab"/>
			<!-- Class -->
			<property name="Class" value="DewCollector"/>
			<property name="MinConvertTime" value="3600" /> <!-- 21600 Game Seconds = 6 Game Hours -->
			<property name="MaxConvertTime" value="7200" /> <!-- 36000 Game Seconds = 10 Game Hours -->
			<property name="ConvertToItem" value="drinkJarRiverWater" />
			<!-- UI_Info -->
			<property name="CustomIcon" value="ZT_SinkClearWood" />
			<property name="DescriptionKey" value="ZTSinkDesc"/>
			<property name="PickupJournalEntry" value="sinkTip"/>
			<property name="WorkstationIcon" value="ui_game_symbol_water"/>
			<property name="WorkstationJournalTip" value="waterTip"/>
			<!--Interaction -->
			<property name="HeatMapStrength" value="1"/>
			<property name="HeatMapTime" value="1200"/>
			<property name="HeatMapFrequency" value="25"/>
			<property name="OpenSound" value="open_workbench"/>
			<property name="CloseSound" value="close_workbench"/>
			<property name="ConvertSound" value="bucketfill_water"/>
			<!-- Destruction_Info -->
			<property class="RepairItems">
				<property name="resourceForgedIron" value="2" />
			</property>
			<drop event="Destroy" name="resourceWood" count="5" />
			<drop event="Destroy" name="resourceScrapIron" count="2,4" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMetalPipe" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
			<!-- Sorting_Order -->
			<property name="Group" value="Building,advBuilding,Basics,Food/Cooking"/>
			<property name="FilterTags" value="MC_building,SC_loot,SC_plumbing"/>
			<property name="SortOrder1" value="ZT1000"/>
			<property name="SortOrder2" value="000022"/>
		</block>
		
		<block name="ZT_SinkDarkWood">
			<property name="Extends" value="ztKitchenParentBlocks"/>
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="250"/>
			<property name="Model" value="#@modfolder:Resources/kitchenModels.unity3d?ZTSink04.prefab"/>
			<!-- Class -->
			<property name="Class" value="DewCollector"/>
			<property name="MinConvertTime" value="3600" /> <!-- 21600 Game Seconds = 6 Game Hours -->
			<property name="MaxConvertTime" value="7200" /> <!-- 36000 Game Seconds = 10 Game Hours -->
			<property name="ConvertToItem" value="drinkJarRiverWater" />
			<!-- UI_Info -->
			<property name="CustomIcon" value="ZT_SinkDarkWood" />
			<property name="DescriptionKey" value="ZTSinkDesc"/>
			<property name="PickupJournalEntry" value="sinkTip"/>
			<property name="WorkstationIcon" value="ui_game_symbol_water"/>
			<property name="WorkstationJournalTip" value="waterTip"/>
			<!--Interaction -->
			<property name="HeatMapStrength" value="1"/>
			<property name="HeatMapTime" value="1200"/>
			<property name="HeatMapFrequency" value="25"/>
			<property name="OpenSound" value="open_workbench"/>
			<property name="CloseSound" value="close_workbench"/>
			<property name="ConvertSound" value="bucketfill_water"/>
			<!-- Destruction_Info -->
			<property class="RepairItems">
				<property name="resourceForgedIron" value="2" />
			</property>
			<drop event="Destroy" name="resourceWood" count="5" />
			<drop event="Destroy" name="resourceScrapIron" count="2,4" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMetalPipe" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
			<!-- Sorting_Order -->
			<property name="Group" value="Building,advBuilding,Basics,Food/Cooking"/>
			<property name="FilterTags" value="MC_building,SC_loot,SC_plumbing"/>
			<property name="SortOrder1" value="ZT1000"/>
			<property name="SortOrder2" value="000036"/>
		</block>
		
		<block name="ZT_BathroomSink2">
			<property name="Extends" value="ztWoodBasedParentBlocks"/>
			<property name="Material" value="MwoodMetal" />
			<property name="MaxDamage" value="250"/>
			<property name="Model" value="#@modfolder:Resources/bathroomModels.unity3d?ZTBath07.prefab"/>
			<!-- Class -->
			<property name="Class" value="DewCollector"/>
			<property name="MinConvertTime" value="3600" /> <!-- 21600 Game Seconds = 6 Game Hours -->
			<property name="MaxConvertTime" value="7200" /> <!-- 36000 Game Seconds = 10 Game Hours -->
			<property name="ConvertToItem" value="drinkJarRiverWater" />
			<!-- UI_Info -->
			<property name="CustomIcon" value="ZT_BathroomSink2" />
			<property name="DescriptionKey" value="ZTSinkDesc"/>
			<property name="PickupJournalEntry" value="sinkTip"/>
			<property name="WorkstationIcon" value="ui_game_symbol_water"/>
			<property name="WorkstationJournalTip" value="waterTip"/>
			<!--Interaction -->
			<property name="HeatMapStrength" value="1"/>
			<property name="HeatMapTime" value="1200"/>
			<property name="HeatMapFrequency" value="25"/>
			<property name="OpenSound" value="open_workbench"/>
			<property name="CloseSound" value="close_workbench"/>
			<property name="ConvertSound" value="bucketfill_water"/>
			<!-- Destruction_Info -->
			<property class="RepairItems">
				<property name="resourceForgedIron" value="2" />
			</property>
			<drop event="Destroy" name="resourceWood" count="5" />
			<drop event="Destroy" name="resourceScrapIron" count="2,4" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Destroy" name="resourceMetalPipe" count="1,3" tag="salvageHarvest" tool_category="Disassemble"/>
			<drop event="Fall" name="resourceWood" count="3" prob="0.20" stick_chance="0" />
			<!-- Sorting_Order -->
			<property name="Group" value="Building,advBuilding,Basics,Food/Cooking"/>
			<property name="FilterTags" value="MC_building,SC_loot,SC_plumbing"/>
			<property name="SortOrder1" value="ZT9000"/>
			<property name="SortOrder2" value="000007"/>
		</block>
 
 
 ================================
 
 
 